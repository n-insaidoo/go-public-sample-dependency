module bitbucket.org/n-insaidoo/go-public-sample-dependency

go 1.12

require (
	github.com/ChimeraCoder/anaconda v1.0.0
	google.golang.org/api v0.29.0
)
